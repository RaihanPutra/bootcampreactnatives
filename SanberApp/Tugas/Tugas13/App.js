import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Settings, StyleSheet, Text, View } from 'react-native';


import Latihan15 from './Latihan/Latihan15/index2';

export default function App()
{
  return (
    <Latihan15 />

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
